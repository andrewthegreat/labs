#include "stdio.h"


struct keyboard {
    unsigned int numLock:1;
    unsigned int capsLock:1;
    unsigned int scrLk:1;
};

union stateCode { 
    struct keyboard state;
    int code;
};

int main(void) {
    union stateCode theCode;
    int value;
    
    scanf("%x", &value);
    theCode.code = value;
    printf("NumLock: %d\n", theCode.state.numLock); //"1" for "ON", "0" for "OFF"
    printf("CapsLock: %d\n", theCode.state.capsLock);
    printf("ScrollLock: %d\n", theCode.state.scrLk);
    return 0;
}
