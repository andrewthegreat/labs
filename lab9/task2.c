#include "stdio.h"


int main(void) {
    //task 2
    int tariffMinutePrice, freeMinutes = 499, overMinutes, overPrice, totalMinutes, totalPrice;

    printf("enter tariff plan (price per minute):\n");
    scanf("%d", &tariffMinutePrice);
    printf("enter total minutes:\n");
    scanf("%d", &totalMinutes);
    printf("enter price per minute (over tariff plan):\n");
    scanf("%d", &overPrice);

    if (overPrice < tariffMinutePrice) {
        int tmp = overPrice;
        overPrice = tariffMinutePrice;
        tariffMinutePrice = tmp;
    }

    if (totalMinutes <= freeMinutes) {
        totalPrice = tariffMinutePrice * totalMinutes;
    }
    else {
        totalPrice = tariffMinutePrice * freeMinutes + (totalMinutes - freeMinutes) * overPrice;
    }
    printf("total price is: %d\n", totalPrice);
    return 0;
}